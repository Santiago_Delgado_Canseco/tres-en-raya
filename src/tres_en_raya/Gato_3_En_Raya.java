package tres_en_raya;
        
import javax.swing.JOptionPane;



public class Gato_3_En_Raya extends javax.swing.JFrame {
    String X="x";
    
    int cont;
    int Counter;
    String JugadorX;
    String JugadorO;

    /** Creates new form Gato_3_En_Raya */
    public Gato_3_En_Raya() {
        initComponents();
    }
    
    
    
    
     public void Ganador(){
        String Uno = jButton1.getText();
        String Dos = jButton2.getText();
        String Tres = jButton3.getText();
        String Cuatro = jButton4.getText();
        String Cinco = jButton5.getText();
        String Seis = jButton6.getText();
        String Siete = jButton7.getText();
        String Ocho = jButton8.getText();
        String Nueve = jButton9.getText();
        
        if(Uno.equals("x")&&Dos.equals("x")&&Tres.equals("x")){
            Gana_X();
        }
        
        if(Cuatro.equals("x")&&Cinco.equals("x")&&Seis.equals("x")){
            Gana_X();
        }
        
        if(Siete.equals("x")&&Ocho.equals("x")&&Nueve.equals("x")){
            Gana_X();
        }
        
        if(Uno.equals("x")&&Cuatro.equals("x")&&Siete.equals("x")){
            Gana_X();
        }
        
        if(Dos.equals("x")&&Cinco.equals("x")&&Ocho.equals("x")){
            Gana_X();
        }
        
        if(Tres.equals("x")&&Seis.equals("x")&&Nueve.equals("x")){
            Gana_X();
        }
        
        if(Tres.equals("x")&&Cinco.equals("x")&&Siete.equals("x")){
            Gana_X();
        }
        
        if(Uno.equals("x")&&Cinco.equals("x")&&Nueve.equals("x")){
            Gana_X();
        }
        
        
        
        
        
        
        
        
        
        if(Uno.equals("O")&&Dos.equals("O")&&Tres.equals("O")){
            Gana_O();
        }
        
        if(Cuatro.equals("O")&&Cinco.equals("O")&&Seis.equals("O")){
            Gana_O();
        }
        
        if(Siete.equals("O")&&Ocho.equals("O")&&Nueve.equals("O")){
            Gana_O();
        }
        
        if(Uno.equals("O")&&Cuatro.equals("O")&&Siete.equals("O")){
            Gana_O();
        }
        
        if(Dos.equals("O")&&Cinco.equals("O")&&Ocho.equals("O")){
            Gana_O();
        }
        
        if(Tres.equals("O")&&Seis.equals("O")&&Nueve.equals("O")){
            Gana_O();
        }
        
        if(Tres.equals("O")&&Cinco.equals("O")&&Siete.equals("O")){
            Gana_O();
        }
        
        if(Uno.equals("O")&&Cinco.equals("O")&&Nueve.equals("O")){
            Gana_O();
        }
    }
    

    public void Gana_X(){
        JOptionPane.showMessageDialog(null, "El Jugado de las X Ganó");
        NuevoJuego();
        cont += 1;
        String cont1 = String.valueOf(cont);
        Jugador1.setText(cont1);
    }
    
    public void Gana_O(){
        JOptionPane.showMessageDialog(null, "El Jugado de las O Ganó");
        NuevoJuego();
        Counter += 1;
        String Counter1 = String.valueOf(Counter);
        Jugador2.setText(Counter1);
    }
    
    public void Empate(){
        String Uno = jButton1.getText();
        String Dos = jButton2.getText();
        String Tres = jButton3.getText();
        String Cuatro = jButton4.getText();
        String Cinco = jButton5.getText();
        String Seis = jButton6.getText();
        String Siete = jButton7.getText();
        String Ocho = jButton8.getText();
        String Nueve = jButton9.getText();
        
        
        if(Uno!=""&&Dos!=""&&Tres!=""&&Cuatro!=""
                &&Cinco!=""&&Seis!=""&&Siete!=""
                &&Ocho!=""&&Nueve!=""){
            JOptionPane.showMessageDialog(null, "Empate");
            NuevoJuego();
        }
    }
    
    public void NuevoJuego(){
        jButton1.setText("");
        jButton2.setText("");
        jButton3.setText("");
        jButton4.setText("");
        jButton5.setText("");
        jButton6.setText("");
        jButton7.setText("");
        jButton8.setText("");
        jButton9.setText("");
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        J1 = new javax.swing.JLabel();
        Jugador1 = new javax.swing.JTextField();
        J2 = new javax.swing.JLabel();
        Jugador2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new java.awt.GridLayout(3, 3));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1);

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2);

        jButton3.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton3);

        jButton4.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton4);

        jButton5.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton5);

        jButton6.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton6);

        jButton7.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton7);

        jButton8.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton8);

        jButton9.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton9);

        jButton10.setText("Borrar Resultado");
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        J1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        J1.setText("Jugador X");

        Jugador1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Jugador1.setText("0");

        J2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        J2.setText("Jugador O");

        Jugador2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Jugador2.setText("0");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(56, 56, 56)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(J2)
                                .addComponent(Jugador2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(Jugador1, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(J1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jButton10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(J1)
                        .addGap(18, 18, 18)
                        .addComponent(Jugador1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(J2)
                        .addGap(18, 18, 18)
                        .addComponent(Jugador2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton10)))
                .addContainerGap(48, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jButton1.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        jButton2.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jButton3.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jButton4.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        jButton5.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        jButton6.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        jButton7.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        jButton8.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        jButton9.setText(X);
        if(X.equals("x")){
            X = "O";
        }
        else
        {
            X="x";
        }
        
        Ganador();
        Empate();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        cont=0;
        Counter=0;
        
        String cont1 = String.valueOf(cont);
        Jugador1.setText(cont1);
        
        String Counter1 = String.valueOf(Counter);
        Jugador2.setText(Counter1);
    }//GEN-LAST:event_jButton10ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Gato_3_En_Raya.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Gato_3_En_Raya.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Gato_3_En_Raya.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Gato_3_En_Raya.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Gato_3_En_Raya().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel J1;
    private javax.swing.JLabel J2;
    private javax.swing.JTextField Jugador1;
    private javax.swing.JTextField Jugador2;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables

}
